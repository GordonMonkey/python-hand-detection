import cv2 
from constants import COLOR_BLUE


class Drawer:
    def __init__(self, image):
        self.image = image
        self.HEIGHT, self.WIDTH, _ = image.shape


    def draw_circle(self, pivot, color):
        cv2.circle(self.image, (pivot["x"], pivot["y"]), 5, color, -1)


    def draw_line(self, start, end, color):
        cv2.line(self.image, (start["x"], start["y"]), (end["x"], end["y"]), color, 2)
        
        
    def get_pivot_position(self, pivot):
        return { "x": int(pivot.x * self.WIDTH), "y": int(pivot.y * self.HEIGHT) }


    def draw_between_hands(self, hands):
        if len(hands) != 2: return
        
        left_hand = hands[0]
        right_hand  = hands[1]
        
        lh_finger_list = left_hand.get_all_fingers_tips()
        rh_finger_list = right_hand.get_all_fingers_tips()

        for l_tip, r_tip in zip(lh_finger_list, rh_finger_list):
            l_pivot = self.get_pivot_position(l_tip)
            r_pivot = self.get_pivot_position(r_tip)
            
            self.draw_line(l_pivot, r_pivot, COLOR_BLUE)
            