import time;
from finger import Finger
from hand import Hand 
import cv2 
import mediapipe as mp 
from timer import Timer
from drawings import Drawer


mp_hands = mp.solutions.hands
hands = mp_hands.Hands(min_detection_confidence=0.5, min_tracking_confidence=0.5)
mp_drawing = mp.solutions.drawing_utils
cap = cv2.VideoCapture(0)



def handle_image(image, results, l_hand_timer: Timer, r_hand_timer: Timer):
    if not results.multi_hand_landmarks: return 
    hands = []
    drawer = Drawer(image)
    
    for idx, hand_landmarks in enumerate(results.multi_hand_landmarks):
        handedness = results.multi_handedness[idx].classification[0].label
        mp_drawing.draw_landmarks(image, hand_landmarks, mp.solutions.hands.HAND_CONNECTIONS)

        
        timer = l_hand_timer if handedness == "Left" else r_hand_timer
        hand = Hand(hand_landmarks.landmark, handedness, image, timer)
        hands.append(hand)
        
        thumb: Finger = hand.THUMB
        index: Finger = hand.INDEX
        
        thumb.draw_tip()
        index.draw_tip()
    
        hand.handle_range()
    
    
    drawer.draw_between_hands(hands)
    
    
   
    

def main():
    start = time.time();
    cap = cv2.VideoCapture(0)
    hands = mp.solutions.hands.Hands()
    left_timer = Timer(0)
    right_timer = Timer(0)
    
    
    print("time elapsed = ",  time.time() - start);
    
    while cap.isOpened():
        success, image = cap.read()
        
        if not success:
            print("Nie udało się pobrać obrazu z kamery.")
            continue

        image = cv2.flip(image, 1)
        image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
        image.flags.writeable = False
        
        results = hands.process(image)
        
        image.flags.writeable = True
        image = cv2.cvtColor(image, cv2.COLOR_RGB2BGR)

        handle_image(image, results, left_timer, right_timer)
        

        cv2.imshow('MediaPipe Hands', image)
        if cv2.waitKey(5) & 0xFF == 27:
            break

    cap.release()
    cv2.destroyAllWindows()
    


if __name__ == "__main__":
    main()
    
    

