from finger import Finger;
from timer import Timer
from drawings import Drawer
from constants import  GESTURE_DETECTION_RANGE, MAX_TIME, COLOR_BLUE, X_DISTANCE_BREAKPOINT, FINGER_WEIGHT, Y_DISTANCE_BREAKPOINT
import requests
import pygame


class Hand:
    def __init__(self, landmark, hand, image, timer: Timer):
        self.hand = hand
        self.__landmark = list(landmark)
        self.__slice_length = 4
        self.__fingers_names = ["THUMB", "INDEX", "MIDDLE", "RING", "PINKY"]
        self.__landmark.pop(0)
        self.__drawer = Drawer(image)
        self.__timer = timer
        self.__set_fingers()
            
    
    def __slice_list(self):
        return [self.__landmark[i:i + self.__slice_length] for i in range(0, len(self.__landmark), self.__slice_length)]

    
    def __get_positions(self):
        fingers_positions = []

        for sublist in self.__slice_list():
            fingers_positions.append({
                "cmc": sublist[0],
                "mcp": sublist[1],
                "ip":  sublist[2],
                "tip": sublist[3],
            })
            
        return fingers_positions
    
    
    def __set_fingers(self):
        fingers_positions = self.__get_positions()
        
        for idx, finger_name in enumerate(self.__fingers_names):
            setattr(self, finger_name, Finger(fingers_positions[idx], self.__drawer))

    
    def __calculate_distance_y(self):
        return self.THUMB.tip.y - self.INDEX.tip.y
    
    
    def __calculate_distance_x(self, initial_x):
        return self.THUMB.tip.x - initial_x
    
    
    def handle_range(self):
        distance_y = self.__calculate_distance_y()
        distance_x = self.__calculate_distance_x(self.__timer.initial_x)
        
        if distance_y < GESTURE_DETECTION_RANGE:
            self.__timer.start();
            self.__timer.initial_x = self.THUMB.tip.x
            
            
        if distance_y > Y_DISTANCE_BREAKPOINT and distance_x > X_DISTANCE_BREAKPOINT:
            total_time = self.__timer.get_total();
            self.__timer.clear();
            
            if total_time > MAX_TIME: return 
            
            #force = calculate_force(distance_y, total_time)
            print(f"time {total_time},  for {self.hand} hand!")

            sound = 'sound.mp3';
            
            # maybe change that
            pygame.init()
            pygame.mixer.init()
            pygame.mixer.music.load(sound)
            pygame.mixer.music.play()
            
          
            
    def draw_line_between_fingers(self):
        thumb_tip = self.__drawer.get_pivot_position(self.THUMB.tip)
        index_tip = self.__drawer.get_pivot_position(self.INDEX.tip)
        pinky_tip = self.__drawer.get_pivot_position(self.PINKY.tip)
        middle_tip = self.__drawer.get_pivot_position(self.MIDDLE.tip)
        
        self.__drawer.draw_line(thumb_tip, index_tip, COLOR_BLUE)
        self.__drawer.draw_line(thumb_tip, pinky_tip, COLOR_BLUE)
        self.__drawer.draw_line(thumb_tip, middle_tip, COLOR_BLUE)
        self.__drawer.draw_line(pinky_tip, middle_tip, COLOR_BLUE)
        
        
        
    def event_handler(self):
        pass
    
    
    def get_all_fingers_tips(self):
        return [self.THUMB.tip, self.INDEX.tip, self.MIDDLE.tip, self.RING.tip, self.PINKY.tip]