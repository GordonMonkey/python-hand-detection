from drawings import Drawer
from constants import COLOR_GREEN


class Finger:
    def __init__(self, positon, drawer: Drawer):
        self.cmc = positon["cmc"]
        self.mcp = positon["mcp"]
        self.ip = positon["ip"]
        self.tip = positon["tip"]
        self.__drawer = drawer
        

        
    def draw_tip(self):
        tip_pivot = self.__drawer.get_pivot_position(self.tip)
        self.__drawer.draw_circle(tip_pivot, COLOR_GREEN)
    
    
