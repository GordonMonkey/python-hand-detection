import time


class Timer:
    def __init__(self, value: float):
        self.value = value 
        self.was_finished = False;
        self.initial_x = 0;
        
        
    def start(self):
        self.value = time.time();
        
        
    def get_total(self):
        if self.was_finished: return;
        self.was_finished = True;
        
        return time.time() - self.value
    
    
    def clear(self):
        self.value = 0;
        self.was_finished = False;
        self.initial_x = 0;